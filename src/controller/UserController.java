package controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import business.AccountBusiness;
import user.User;

@ManagedBean
@RequestScoped
public class UserController {

	@Inject
	AccountBusiness ab;
	
	public String onLogin(User user) {
		if(ab.AuthenticateUser(user) == 1) {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", ab.getUserData().findByUserName(user));
			return "TestResponse.xhtml?faces-redirect=true";
		}
		else {
			return "Login.xhtml";
		}
	}

	public String onRegister(User user) {
		if(ab.RegisterUsers(user) == 1) {
			return "Login.xhtml";
		} else {
			return "Register.xhtml";
		}
	}

	public String updateUserInfo(User u) {
		ab.updateUserInfo(u);
		return "UserInfo.xhtml";
	}
}
