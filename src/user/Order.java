package user;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="order")
@ManagedBean
@ViewScoped
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id = 0;
	private String wordNo = "";
	private String wordName = "";
	private int quantity = 0;
	
	public Order() {
		
	}
	
	public Order(int id, String wordNo, String wordName, int quantity) {
		this.id = id;
		this.wordNo = wordNo;
		this.wordName = wordName;
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getWordNo() {
		return wordNo;
	}

	public void setWordNo(String wordNo) {
		this.wordNo = wordNo;
	}

	public String getWordName() {
		return wordName;
	}

	public void setWordName(String wordName) {
		this.wordName = wordName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
