package user;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Orders {

List<Order> orders = new ArrayList<Order>();
	
	public Orders() {
		orders.add(new Order(0, "00000", "Onomatopoeia", 1));
		orders.add(new Order(1, "00001", "GCU", 13));
		orders.add(new Order(2, "00002", "Cameron", 5));
		orders.add(new Order(3, "00003", "Bryant", 5));
		orders.add(new Order(4, "00004", "Phoenix", 10));
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}
