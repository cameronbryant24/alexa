package business;

import javax.ejb.Local;

import user.User;

@Local
public interface AccountBusinessInterface {

	public int RegisterUsers(User user);
	
	public int AuthenticateUser(User user);
	
	String hashPassword(String pass);
}
