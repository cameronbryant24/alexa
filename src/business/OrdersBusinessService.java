package business;

import java.sql.Connection;
import java.util.List;
import java.util.Queue;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import data.OrderDataService;
import user.Order;

@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
public class OrdersBusinessService implements OrdersBusinessInterface {

	@Inject
	OrderDataService orderDataService;

	@Resource(mappedName="java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;

//	@Resource(mappedName="java:/jms/queue/Order")
//	private Queue queue;
	
	public List<Order> getOrders() {
		//return all orders from the order data service
		return orderDataService.findAll();
	}
    public OrdersBusinessService() {

    }
    
    public Order getOrderById(int orderId) {
    	return orderDataService.findById(orderId);
    }

	public void setOrders(Order order) {
		// TODO Auto-generated method stub
	}

    public void deleteOrder(Order order) {
    	orderDataService.deleteT(order);
    }
    
    public void saveOrder(Order order) {
    	orderDataService.createT(order);
    }
    
    public void updateOrder(Order order) {
    	orderDataService.updateT(order);
    }
    
    public void sendOrder(Order order) {
    	try {
    		//Get a connection and session to the JMS Connection Factory
    		javax.jms.Connection connection = connectionFactory.createConnection();
    		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    		
    		//Create a message producer for sending messages to the queue
    		//MessageProducer messageProducer = session.createProducer((Destination) queue);
    		
    		//Create and send a text message
    		TextMessage message1 = session.createTextMessage();
    		message1.setText("This is test message");
    		//messageProducer.send(message1);
    		
    		//Create and send an object message (with the order)
    		ObjectMessage message2 = session.createObjectMessage();
    		message2.setObject(order);
    		//messageProducer.send(message2);
    		
    		//Clean up by closing the connection to the JMS connection factory
    		connection.close();
    	}
    	catch (JMSException e) {
    		e.printStackTrace();
    	}
    }
	@Override
	public void test() {
		// TODO Auto-generated method stub
		
	}
}
