package data;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DataAccessInterface <T> {

	public List<T> findAll();
	public boolean createT(T t);
	public boolean updateT(T t);
	public boolean deleteT(T t);
	public T findById(int id) throws Exception;
}
