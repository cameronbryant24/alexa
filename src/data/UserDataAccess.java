package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;

import exception.UserNotFoundException;
import user.User;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class UserDataAccess {

	private Connection conn = null;
	private String url = "jdbc:mysql://localhost:3306/keyobserver";
	private String username = "root";
	private String password = "9286148990";
	
	public List<User> findAll() {
		return null;
	}

	public User findById(int id) throws UserNotFoundException {
		String sql = String.format("SELECT * FROM `keyobserver`.`user` WHERE `USER_ID`= %i",
				id);
		User dataUser = new User();
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			dataUser.setFirstName(rs.getString("FIRSTNAME"));
			dataUser.setLastName(rs.getString("LASTNAME"));
			dataUser.setUserName(rs.getString("USERNAME"));
			dataUser.setEmail(rs.getString("EMAIL"));
			dataUser.setPassword(rs.getString("PASSWORD"));
			dataUser.setPhoneNumber(rs.getString("PHONENUMBER"));
			return dataUser;
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new UserNotFoundException();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean createT(User t) {
		try {
			String sql = String.format("INSERT INTO `keyobserver`.`user` "
					+ "(`FIRSTNAME`, `LASTNAME`, `USERNAME`, `EMAIL`, `PASSWORD`,`PHONENUMBER`) "
					+ "VALUES ('%s', '%s', '%s', '%s', '%s', '%s');",
					t.getFirstName(),
					t.getLastName(),
					t.getUserName(),
					t.getEmail(),
					t.getPassword(),
					t.getPhoneNumber());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			String sql2 = "SELECT LAST_INSERT_ID() AS `LAST_ID` FROM `user`;";
			ResultSet rs = stmt.executeQuery(sql2);
			rs.next();
			int userID = rs.getInt("LAST_ID");
			stmt.close();
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("userID", userID);
			
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean updateT(User t) {
		try {
			String sql = String.format("UPDATE `keyobserver`.`user` SET `FIRSTNAME` = '%s', `LASTNAME` = '%s', `USERNAME` = '%s', `EMAIL` = '%s', `PASSWORD` = '%s', `PHONENUMBER` = '%s' WHERE `USER_ID` = %d",
					t.getFirstName(),
					t.getLastName(),
					t.getUserName(),
					t.getEmail(),
					t.getPassword(),
					t.getPhoneNumber());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean deleteT(User t) {
		String sql = String.format("DELETE FROM `keyobserver`.`user` WHERE `USER_ID` = %d",
				t.getUserName());
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User findByUserName(User t) {
		try {
			String sql = String.format("SELECT * FROM `keyobserver`.`user` WHERE `USERNAME`= '%s';",
					t.getUserName());
			User dataUser = new User();
			
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			dataUser.setFirstName(rs.getString("FIRSTNAME"));
			dataUser.setLastName(rs.getString("LASTNAME"));
			dataUser.setUserName(rs.getString("USERNAME"));
			dataUser.setEmail(rs.getString("EMAIL"));
			dataUser.setPassword(rs.getString("PASSWORD"));
			dataUser.setPhoneNumber(rs.getString("PHONENUMBER"));
			return dataUser;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return new User();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
