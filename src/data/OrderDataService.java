package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import user.Order;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class OrderDataService implements DataAccessInterface<Order> {

	private Connection conn = null;
	private String url = "jdbc:mysql://localhost:3306/keyobserver";
	private String username = "root";
	private String password = "9286148990";
	
	public OrderDataService() {
		
	}
	
	public List<Order> findAll() {
		
		String sql = "Select * FROM ORDERS";
		List<Order> orders = new ArrayList<Order>();
		try {
			//connect to the database
			conn = DriverManager.getConnection(url, username, password);
			
			//execute SQL Query and loop over result set
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				orders.add(new Order(rs.getInt("ID"), 
						rs.getString("WORD_NO"), 
						rs.getString("WORD_NAME"),
						rs.getInt("QUANTITY")));
			}
			
			//cleanup result set
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			//cleanup database
			if(conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return orders;
	}

	public Order findById(int id) {
		
		String sql = String.format("SELECT * FROM `keyobserver`.`orders` WHERE `ID`= %d;", id);
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			Order order = new Order();
			order.setId(rs.getInt(1));
			order.setWordNo(rs.getString("WORD_NO"));
			order.setWordName(rs.getString("WORD_NAME"));
			order.setQuantity(rs.getInt(1));
			return order;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return new Order();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean createT(Order t) {
		
		Connection conn = null;
		try {
			String sql = String.format("INSERT INTO `keyobserver`.`orders`(WORD_NO, WORD_NAME, QUANTITY) VALUES('%s', '%s', %d)", 
					t.getWordNo(), 
					t.getWordName(), 
					t.getQuantity());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			//really should check the return value from executeUpdate
			stmt.executeUpdate(sql);
			stmt.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if(conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public boolean updateT(Order t) {
		
		try {
			String sql = String.format("UPDATE `keyobserver`.`orders` SET `WORD_NO` = '%s', `WORD_NAME` = '%s', `QUANTITY` = %d where ID=%d;",
					t.getWordNo(),
					t.getWordName(),
					t.getQuantity(),
					t.getId());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean deleteT(Order t) {
		String sql = String.format("DELETE FROM `keyobserver`.`orders` WHERE `ID` = %d;",
				t.getId());
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
